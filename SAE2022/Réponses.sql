--1) Les pièces spécifiques 
--à cette boîte (nom, quantité, catégorie, couleur)

select nompiece , quantitep, nomcat, nomcoul
from PIECE natural join CATEGORIE natural join CONTENIR
natural join COULEUR natural join CONTENU 
natural join BOITE
where numboite='10201-1';

--2 Les boîtes incluses dans cette boîte

select nomboite
from INCLURE natural join BOITE
where idcont=(select idcont from CONTENU where numboite='10201-1');

--3. La liste des pièces (nom, quantité, catégorie, couleur) qui sont en quantité supérieure à 10
--dans ces boîtes incluses


select nompiece, quantitep, nomcat, nomcoul
from PIECE natural join CATEGORIE natural join COULEUR natural join CONTENIR natural join CONTENU natural join BOITE
where numboite="8593-1" or numboite="8596-1" 
having quantitep>=10;

--4. La liste des pièces en supplément des sous-boîtes (nom, quantité, catégorie, couleur)

select nompiece, en_supplement
from PIECE natural join CATEGORIE natural join COULEUR natural join CONTENIR 
natural join CONTENU natural join BOITE 
where numboite="8593-1" or numboite="8596-1" 
having en_supplement="t";



-- Nombre de piece par boite 
select sum(nbpieces)
from INCLURE natural join BOITE
where idcont=(select idcont from CONTENU where numboite='10201-1');

-- SAE2.04 1
-- Nom: jacquet axel 

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations sur les boîtes qui contiennent une pièce de couleur Very Light Orange

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+----------------------------+-------+----------+---------+
-- | numboite | nomboite                   | annee | nbpieces | idtheme |
-- +----------+----------------------------+-------+----------+---------+
-- | 5823-1   | The Good Fairy's Bedroom   | 2000  | 33       | 319     |
-- | etc...
-- = Reponse question 1.


select numboite, nomboite, annee,nbpieces, idtheme
from BOITE natural join CONTENIR natural join PIECE natural join CONTENU
natural join COULEUR
where nomcoul="Very Light Orange";

-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations sur les contenus qui sont présents à la fois dans INCLURE et dans CONTENIR.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +--------+---------+-------------+
-- | idcont | version | numboite    |
-- +--------+---------+-------------+
-- | 87     | 1       | 5001925-1   |
-- | etc...
-- = Reponse question 2.

select distinct idcont, version, numboite
from CONTENU natural join BOITE natural join CONTENIR
where idcont in (select idcont from INCLURE);



-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations des pièces qui ne sont utilisées que dans des boîtes du thème Jungle.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +--------------+----------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | numpiece     | nompiece                                                                                                                                           | idcat |
-- +--------------+----------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | 2454px6      | Brick 1 x 2 x 5 with Jungle Print                                                                                                                  | 2     |
-- | etc...
-- = Reponse question 3.


select distinct numpiece, nompiece
from PIECE natural join CONTENIR natural join CONTENU
natural join BOITE natural join THEME
where nomtheme="Jungle" and numpiece not in (select numpiece
from PIECE natural join CONTENIR natural join CONTENU
natural join BOITE natural join THEME
where nomtheme<>"Jungle")
ORDER BY numpiece;

------------------------------------------------------Demander car pas sure sans le order by 



-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Les informations sur la ou les pièces utilisées en plus grande quantité dans une seule boîte.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+-------------+-------+
-- | numpiece | nompiece    | idcat |
-- +----------+-------------+-------+
-- | 3024     | Plate 1 x 1 | 14    |
-- +----------+-------------+-------+
-- = Reponse question 4.

select numpiece, nompiece, idcat
from PIECE natural join CONTENIR 
where quantitep >= ALL(select quantitep
from PIECE natural join CONTENIR);

-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  L'année où pour la première fois une boîte du thème Technic est apparue.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------+
-- | annee |
-- +-------+
-- | 1973  |
-- +-------+
-- = Reponse question 5.


select min(annee)
from BOITE natural join THEME
where nomtheme="Technic";



-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Pour chaque année, on voudrait connaitre le nombre de couleurs différentes utilisées dans les boîtes crées cette année là.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------+--------+
-- | annee | nbcoul |
-- +-------+--------+
-- | 1950  | 10     |
-- | etc...
-- = Reponse question 6.

select annee, count(distinct nomcoul ) nbcoul
from BOITE natural join CONTENU natural join CONTENIR natural join COULEUR
group by annee;


-- +------------------+--
-- * Question 7 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  les informations des pièces qui ne sont contenues dans aucune boîte.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | numpiece       | nompiece                                                                                                                                                                                        | idcat |
-- +----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------+
-- | 10             | Baseplate 24 x 32                                                                                                                                                                               | 1     |
-- | etc...
-- = Reponse question 7.

select numpiece, nompiece
from PIECE
where numpiece not in (select numpiece from CONTENIR);


--------------------------------------------------------------- Demander à MR limet car bcp de res

-- +------------------+--
-- * Question 8 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  la requête qui associe à chaque identifiant de thème le nom de son thème principal.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +---------+----------------------------------+
-- | idtheme | nomtheme                         |
-- +---------+----------------------------------+
-- | 1       | Technic                          |
-- | etc...
-- = Reponse question 8.

select idtheme , nomtheme
from THEME
where idtheme_1 is NULL
group by idtheme


----------------------------------------------- A demander


-- +------------------+--
-- * Question 9 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  la requête qui donne les informations des boîtes du thème Jungle qui utilisent plus de 100 pièces différentes. 
-- Deux pièces sont identiques si elles ont la même référence et la même couleur.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+--------------------------+-------+----------+---------+----------+
-- | numboite | nomboite                 | annee | nbpieces | idtheme | nbpieces |
-- +----------+--------------------------+-------+----------+---------+----------+
-- | 5976-1   | River Expedition         | 1999  | 318      | 299     | 173      |
-- | etc...
-- = Reponse question 9.


select numboite, nomboite, annee, sum(quantitep) nbpieces, idtheme, count(distinct nompiece) nbpieces
from BOITE natural join CONTENIR natural join CONTENU natural join PIECE natural join THEME
where nomtheme="jungle" and nbpieces >100
group by numboite;


-- +-------------------+--
-- * Question 10 :     --
-- +-------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  la requête qui permet de retrouver le contenu complet d'un CONTENU 
-- incluant son propre contenu ainsi que celui des boîtes qui le composent.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +--------+-----------------+--------+-----------+---------------+
-- | idcont | numpiece        | idcoul | quantitep | en_supplement |
-- +--------+-----------------+--------+-----------+---------------+
-- | 1      | 48379c01        | 72     | 1         | f             |
-- | etc...
-- = Reponse question 10.



