-- Création de la nouvelle Boite IUT'O

insert into  BOITE(numboite,nomboite,annee, nbpieces, idtheme) values
    ('753159',"IUT'O",2022,6,50);
    

-- Creation de la relation entre la boite et son contenu.

insert into CONTENU(idcont,version,numboite) values
    (123456,1,"753159");

--Ajouts des différentes pieces;

insert into CONTENIR(`idcont`,`numpiece`,`idcoul`,`quantitep`,`en_supplement`) values
    (123456,'bear',379,3,'F'),
    (123456,'33121',100,1,'F'),
    (123456,'kragle',-1,1,'F' ),
    (123456,'kragle',-1,1,'T' );

